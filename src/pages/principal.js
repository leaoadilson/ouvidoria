import React, { Component } from 'react';

import { BackgroundImage } from '../config/GlobalStyles'

import {
    StyleSheet,
    Button,
    View,
    SafeAreaView,
    Text,
    Image
} from 'react-native';

export default class Principal extends Component {
    static navigationOptions = {
        header: null,
    }
    render() {
        const { navigate } = this.props.navigation;
        return (

            <SafeAreaView style={styles.container}>
                <BackgroundImage>
                    <View style={styles.centro}>
                        <View>
                            <Image
                                style={{ width: 260, height: 260 }}
                                source={require('../imgs/arcon.png')}
                            />
                            <Text></Text>
                            <View>
                                <Button
                                    title="INICIAR"
                                    onPress={() => navigate('BoasVindas')}
                                />
                            </View>
                        </View>
                    </View>
                </BackgroundImage>
            </SafeAreaView>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //backgroundColor: "#99ccff",
    },
    centro: {
        alignItems: 'center',
        paddingTop: "35%",
        opacity:1
    }
});