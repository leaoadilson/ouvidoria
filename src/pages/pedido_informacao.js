import React, { Component } from 'react';

import Constants from 'expo-constants';

import { Input } from 'react-native-elements';

import { Header } from 'react-navigation';

import {
    StyleSheet,
    View,
    SafeAreaView,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
    KeyboardAvoidingView,
    ScrollView,
    Text
} from 'react-native';


export default class PedidoInformacao extends Component {
    static navigationOptions = {
        title: 'PEDIDO DE INFORMAÇÃO'
    }
    constructor(props) {
        super(props);
        this.state = {
            nome: '',
            email: '',
            contato: '',
            solicitacao: '',
            isLoading: false
        }
        this._logar = this._logar.bind(this)
    }
    _logar = (mensagem) => {
        console.log(mensagem)
    }
    handlePress = async () => {
        try {
            const dadosEnvio = {
                nomeDenunciante: this.nome,
                contatoDenunciante: this.contato,
                emailDenunciante: this.email,
                solicitacao: this.solicitacao
            };
            // this.setState({
            //     isLoading: true,
            //     dataSource: responseJson.movies,
            //   }, function(){

            // });
            fetch('http://localhost:3005/api/pedidos_de_informacao', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(dadosEnvio)
            })
                .then((response) => response.json())
                .then((responseJson) => {


                    // Alert.alert(
                    //     'Solicitação enviada' + responseJson[0].nome,
                    //     'De acordo com o Decreto 113 de 23 de maio de 2019, a ouvidoria tem o prazo de até 30 (trinta) dias para responder às manifestações de maneira objetiva e conclusiva',
                    //     [
                    //         { text: 'OK', onPress: () => { navigate('Manifestacoes'); console.log('OK Pressionado') } },
                    //     ],
                    //     { cancelable: false },
                    // )
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (error) {
            console.error(error);
        }

    };
    render() {
        const { navigate } = this.props.navigation;

        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView
                    keyboardVerticalOffset = {-200} 
                    style={{ flex: 1 }}
                    behavior='padding'
                    enabled={true}
                >
                    <ScrollView style={{ flex: 1 }}>
                        <View style={styles.conteudo}>
                            <Input
                                label='Nome'
                                value={this.state.nome}
                                onChangeText={nome => this.setState({ nome })}
                            />
                            <Input
                                onChangeText={email => this.setState({ email })}
                                value={this.state.email}
                                containerStyle={styles.entradaTexto}
                                label='E-mail'
                            />
                            <Input
                                onChangeText={contato => this.setState({ contato })}
                                value={this.state.contato}
                                containerStyle={styles.entradaTexto}
                                label='Telefone'
                            />
                            <Input
                                value={this.state.solicitacao}
                                onChangeText={solicitacao => this.setState({ solicitacao })}
                                containerStyle={styles.entradaTexto}
                                editable
                                label='Solicitação'
                                maxLength={480}
                                multiline
                            //numberOfLines={5}
                            />
                            <View style={styles.baixo}>
                            <TouchableOpacity style={styles.botao}>
                                <Text style={styles.textBotao}>ENVIAR</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //marginTop: Constants.statusBarHeight,
        backgroundColor: "#99ccff",
    },
    conteudo: {
        margin: 15,
        paddingTop: 10
    },
    bottom: {
        alignItems: 'center',
        marginHorizontal: 65
    },
    entradaTexto: {
        marginTop: 25
    },
    botao: {
        backgroundColor: '#008ae6',
        borderRadius: 10,
        width: 300,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
        shadowOffset: { height: 4, width: 4 }, // IOS
        shadowOpacity: 1, // IOS
        shadowRadius: 1, //IOS
        elevation: 2, // Android
    },
    textBotao: {
        fontSize: 25,
        color: 'white',
        fontWeight: 'bold'
    },
    baixo:{
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        marginTop:70
    }
});