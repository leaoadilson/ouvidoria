import React, { Component } from 'react';

import Constants from 'expo-constants';

import {
    StyleSheet,
    View,
    SafeAreaView,
    Text,
    TouchableOpacity
} from 'react-native';


export default class Manifestacoes extends Component{
    static navigationOptions = {
        title: 'MANIFESTAÇÕES'
    }
    render(){
        const { navigate } = this.props.navigation;
        return(
            <SafeAreaView style={styles.container}>
            <View style={styles.colunaAplicativo}>

                <View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => navigate('PedidoInformacao')}
                    >
                        <Text style={styles.textoBotao}> Pedido de Informação </Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => navigate('')}
                    >
                        <Text style={styles.textoBotao}> Denúncia </Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => navigate('')}
                    >
                        <Text style={styles.textoBotao}> Reclamação </Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => navigate('')}
                    >
                        <Text style={styles.textoBotao}> Elogio </Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <TouchableOpacity
                        style={[styles.button]}
                        onPress={() => navigate('')}
                    >
                        <Text style={styles.textoBotao}> Sugestão </Text>
                    </TouchableOpacity>
                </View>

            </View>
            
    </SafeAreaView>
        )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
        backgroundColor: "#99ccff",
    },
    button:{
        marginBottom: 26,
        backgroundColor:'#008ae6',
        borderRadius:10,
        width: 340,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(0,0,0, .4)', // IOS
        shadowOffset: { height: 4, width: 4 }, // IOS
        shadowOpacity: 1, // IOS
        shadowRadius: 1, //IOS
        elevation: 2, // Android
    },
    textoBotao:{
        fontSize: 25,
        color:'white'
    },
    colunaAplicativo:{
        marginTop: 20,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignContent: 'space-around',
        alignItems: 'stretch',
        flexWrap:'wrap'
    }
});