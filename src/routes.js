import { createStackNavigator } from 'react-navigation'

import Principal from './pages/principal';
import BoasVindas from './pages/boasvindas';
import Manifestacoes from './pages/manifestacoes';
import PedidoInformacao from './pages/pedido_informacao';


export default createStackNavigator({
    Principal,BoasVindas,Manifestacoes,PedidoInformacao
}, {navigationOptions: {

    headerStyle:{backgroundColor:'#ed1e07'},
    
    headerTintColor:'#FFF'
}});

