'use strict';
import React, { Component } from 'react'
import {
  StyleSheet,
  Image,
  ImageBackground,
  SafeAreaView
} from 'react-native';

export class BackgroundImage extends Component {

  render() {
      return (
        <SafeAreaView style={styles.container}>
          <ImageBackground source={require('../imgs/brasao_para.png')}
                style={styles.backgroundImage}>

                {this.props.children}

          </ImageBackground>
          </SafeAreaView>
      )
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
      height:"100%",
      opacity:0.2
      
  },
  container:{
    flex:1,
    backgroundColor: 'rgb(153, 204, 255)',
    opacity:1
  }
});
// module.exports = StyleSheet.create({

// fundoEstadoPara: {
//   flex: 1,
//   backgroundColor: "#99ccff",

// },
// backgroundImage: {
//   flex: 1,
//   width: null,
//   height: null,
//   resizeMode: 'cover',
//   backgroundColor: "#99ccff",
// },

// });